const mongoose = require('mongoose');
const Schema = mongoose.Schema;

mongoose.connect('mongodb://localhost:27017/test', {useNewUrlParser: true});

var PlantInventoryEntrySchema = new Schema({
    name: String,
    description: String, 
    price: Number
});

var PlantInventoryItemSchema = new Schema({
    serial_number: String,
    equipment_condition: String,
    catalog_entry_id: {type: Schema.Types.ObjectId, ref: 'PlantInventoryEntry'}
});

var PurchaseOrderSchema = new Schema({
    start_date: Date,
    end_date: Date,
    total: Number,
    catalog_entry_id: {type: Schema.Types.ObjectId, ref: 'PlantInventoryEntry'},
    plant_id: {type: Schema.Types.ObjectId, ref: 'PlantInventoryItem'}
});

var PlantInventoryEntry = mongoose.model('PlantInventoryEntry', PlantInventoryEntrySchema);
var PlantInventoryItem = mongoose.model('PlantInventoryItem', PlantInventoryItemSchema);
var PurchaseOrder = mongoose.model('PurchaseOrder', PurchaseOrderSchema);

module.exports = {
    seed_database: function() {
        PlantInventoryEntry.create([
            {name:'Mini excavator', description:'1.5 Tonne Mini excavator', price:100},
            {name:'Mini excavator', description:'3 Tonne Mini excavator', price:200},
            {name:'Midi excavator', description:'5 Tonne Midi excavator', price:250},
            {name:'Midi excavator', description:'8 Tonne Midi excavator', price:300},
            {name:'Maxi excavator', description:'15 Tonne Large excavator', price:400},
            {name:'Maxi excavator', description:'20 Tonne Large excavator', price:450},
            {name:'HS dumper', description:'1.5 Tonne Hi-Swivel Dumper', price:150},
            {name:'FT dumper', description:'2 Tonne Front Tip Dumper', price:180},
            {name:'FT dumper', description:'2 Tonne Front Tip Dumper', price:200},
            {name:'FT dumper', description:'2 Tonne Front Tip Dumper', price:300},
            {name:'FT dumper', description:'3 Tonne Front Tip Dumper', price:400},
            {name:'Loader', description:'Hewden Backhoe Loader', price:200},
            {name:'D-Truck', description:'15 Tonne Articulating Dump Truck', price:250},
            {name:'D-Truck', description:'30 Tonne Articulating Dump Truck', price:300}
          ]).then(plants => {
            console.log("Catalog entries", JSON.stringify(plants));
        
            PlantInventoryItem.create([
                {catalog_entry_id:plants[0], serial_number:'A01', equipment_condition:'SERVICEABLE'},
                {catalog_entry_id:plants[0], serial_number:'A02', equipment_condition:'SERVICEABLE'},
                {catalog_entry_id:plants[1], serial_number:'A03', equipment_condition:'SERVICEABLE'}
            ]).then(items => {
                console.log("Inventory items", JSON.stringify(items));
        
                PurchaseOrder.create({
                    catalog_entry_id:plants[0], 
                    plant_id:items[0], 
                    start_date:new Date(2019, 4, 6), 
                    end_date:new Date(2019, 4, 10)
                }).then(orders => console.log("Purchase orders", JSON.stringify(orders)));
            });
          });
    },
    query_catalog: function() {
        return PlantInventoryEntry.find();
    }
};