var express = require('express');
var router = express.Router();
var sales = require('../models/sales');

/* GET catalog listing. */
router.get('/', function(req, res, next) {
    sales.query_catalog().exec((err, result) => {
        console.log("Result", result);
        res.json(result);
    })
});

module.exports = router;
